from django.contrib import admin
from todos.models import TodoList, TodoItem

class TodoListAdmin(admin.ModelAdmin):
    list = ('id', 'name')

class TodoItemAdmin(admin.ModelAdmin):
    list = ('task', 'due_date', 'iscompleted')

# Register your models here.
admin.site.register(TodoList, TodoListAdmin)
admin.site.register(TodoItem, TodoItemAdmin)
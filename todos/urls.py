from django.urls import path
from .views import todo_list_view

urlpatterns = [
    path('', todo_list_view, name='todos_list_view'),
]
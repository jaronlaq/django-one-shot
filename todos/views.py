from django.shortcuts import render
from .models import TodoList

# Create your views here.
def todo_list_view(request):
    todo_lists = TodoList.objects.all()
    return render(request, 'todos/todo_list_view.html', {'todo_lists': todo_lists})